---
date: 2018-09-28
title: "Speed Boat orienté solution"
sorted: speed boat oriente solution
tags:
  - embarquement
  - coaching
  - atelier
  - Solution Focus
  - Speed Boat
objectives: |
  - Avoir une vision de "où souhaite aller l'équipe ?"
  - Créer une vision commune
  - Partage de futur préféré
  - Embarquer une équipe
duration: 1 à 2 heures
participants: "jusqu'à 15 - L'équipe"
materials:
authors:
  - quille_julie
illustration:
  name: speedsf2.jpg
---

# Présentation

Vous voulez embarquer une équipe de manière ludique et positive ? Nous vous proposons ce speedboat revisité avec Solution Focus. L'objectif est de faire le bilan sur "Où souhaitons-nous aller en tant qu'équipe ?", "Qu'avons-nous déjà réussi jusqu'à présent ?", "Quelle est notre prochaine étape ?".

# Préparation en amont

En amont nous vous proposons de préparer la frise sur laquelle les post-it vont être collés.
Ci-dessous une proposition de frise facile :
En deux parties, sur deux feuilles de paperboard en format paysage :
Sur la feuille de droite dessiner :
- sur la moitié droite une île qui matérialise où nous souhaitons arriver
- sur la moitié gauche une grosse croix qui matérialise la prochaine étape
- en plus : vous pouvez mettre des rochers qui représentent les obstacles que nous aurons à éviter (mais dont nous ne parlerons pas ici), ainsi qu'un chemin de navigation avec de petites croix qui matérialisent le fait que le chemin se fait en de nombreuses étapes

Sur la feuille gauche dessiner :
- à droite un gros bateau qui matérialise où en est l'équipe
- à gauche un quai d'embarquement qui matérialise d'où nous sommes partis
- en plus : idem feuille de droite

Si vous avez des talents de dessinateur, n'hésitez pas à ajouter les détails que vous souhaitez.

Avant le début de l'activité ne laissez que la moitié droite apparente (celle avec l'île)

# Introduction 10 min

Avant de commencer l'atelier il est possible de mettre en mouvement l'équipe avec un energizer ou jeu de votre choix.
Si vous n'avez pas encore votre préférence nous vous proposons le [FlowGame (ou Jeu desTrois balles)](https://azae.net/articles/trois-balles/) qui permet à l'équipe de se challenger et de se mettre dans un mindset d'amélioration continue.

# Déroulé

Départ : seule la partie avec l'île est affichée
1. "Imaginez que vous vous endormez ce soir, tranquillement. Pendant que vous dormez un miracle se produit et vous devenez l'équipe idéale, celle dont vous avez toujours rêvé. Demain matin en vous réveillant que voyez-vous de différent ?"
Durant cette étape l'objectif est de faire rêver l'équipe à son objectif et de relever les détails de ce qui va changer. Nous sommes en phase créative, aucun besoin de consensus à ce stade. C'est également le moment où vous pouvez donner une orientation à l'espace de réflexion : axons-nous nos rêves sur la technique ? La dynamique de groupe ? La communication ? Suivant les priorités identifiées et le cadre de votre intervention vous pouvez choisir d'utiliser ou non cette possibilité d'orienter la conversation.
2. Chacun présente ses post-it au reste de l'équipe et va les coller sur l'île
3. Afficher le quai et le bateau
4. "Vous êtes déjà en route vers cette île. Qu'est-ce qui fait que vous êtes déjà là où vous êtes ? A quoi voyez-vous que vous avez quitté le quai ? Quelles sont vos forces et les forces de l'équipe qui vous permettent d'être déjà là ?"
L'objectif ici est que l'équipe prenne conscience de ses points forts et de tout son potentiel actuel. Il est important de rester dans tout ce qui fonctionne. Durant cet atelier nous ne parlons à aucun moment de ce qui ne fonctionne pas.
5. Chacun présente ses post-it au reste de l'équipe et va les coller sur le bateau
6. "Maintenant imaginez la prochaine étape (vous n'êtes pas encore arrivés mais vous avez avancé), qu'est-ce que vous voyez de différent ? Quoi d'autre ?"
7. Chacun présente ses post-it au reste de l'équipe
8. A partir de tous les post-it regroupés par thématique, faire ressortir les 2 ou 3 axes principaux de ce sur quoi l'équipe a envie de travailler.
9. Vérifier que chacun se retrouve dans les 2 ou 3 thématiques choisies.
10. Coller ces post-it sur la croix "prochaine étape"

Optionnel :
11. Il est possible de prendre un temps d'exploration de ce qui pourrait être fait pour aller vers ces objectifs. Nous sommes en phase de créativité. Il est important de spécifier que nous ne sommes pas en phase de prise d'engagement.

# Bilan

En tant que coach, cet atelier nous permet de donner une orientation à l'accompagnement. Nous avons ici une sorte de premier engagement sur ce vers quoi l'équipe souhaite aller. Ces objectifs de la prochaine étape peuvent être affichés dans l'espace de travail, rappelés régulièrement, où encore servir de focus lors des rétrospectives et autres moments de prise de décision.

A vous de jouer, de tester et de personnaliser cet atelier. N'hésitez pas à nous faire des retours pour nous dire ce que vous avez essayé et comment vous avez vécu l'atelier.
