---
date: 2018-10-25
sorted:
title: "Donner du feedback positif"
draft : false
tags:
- feedback
- coaching
- atelier
- CNV
- dynamique d'équipe
- responsabilisation individuelle
objectives: |
  - Travailler la dynamique de groupe
  - Apprendre à éviter les non-dits
  - S'entrainer au feedback
  - Travailler sur la reconnaissance au sein de l'équipe
  - Travailler sur la responsabilité individuelle
  - Muscler le chapeau jaune
duration: 1 heure
participants: "1 à 15 personnes"
materials:
authors:
  - quille_julie
illustration:
  name: feedbackpositif.jpg

---

# Présentation

Cet atelier a pour objectif de réintroduire le feedback au sein d'une équipe. Il peut se jouer de plusieurs manières différentes, mais l'idée reste la même : donner du feedback positif et voir ce que cela modifie au sein de l'équipe.
C'est une très bonne première étape pour réussir à aller vers plus de conversations constructives (explicitation des attentes, rétros utiles etc.) et de responsabilisation individuelle. C'est également un très bon exercice (à pratiquer au quotidien) pour muscler son [chapeau jaune](http://ajiro.fr/games/six_hats/) ;-)

# Déroulé

Premier temps :
1. Demander ce qu'est et ce que n'est pas le feedback pour les participants.
Bien différencier tout ce qui renvoie à la notion de donner de l'information (après une absence, sur le suivi d'un projet, sur les attentes "factuelles" que nous avons vis à vis de nos collègues) du feedback constructif tel que nous l'entendons.
2. Compléter, commenter la réponse. Distinguer le "bon feedback" (feedback constructif) du "feedback positif" (feedback sur des choses qui nous ont été agréables).
Deuxième temps :
3. Présenter un ou plusieurs modèles de feedback (OSBD en CNV, OSCAR etc.) Vous pouvez vous aider de l'article suivant de [Bloculus](https://bloculus.com/sauvez-des-vies-faites-des-feedbacks/).
Troisième étape :
4. S'entrainer au feedback positif :
Question 1 :
  - Écrire un feedback positif pour chacune des autres personnes de mon équipe.
  - Partager un à un les feedbacks, attention seulement à ceux qui le souhaitent.
  - Les autres personnes (dont le coach) peuvent dire ce qu'elles en pensent, aimeraient-elles recevoir ce genre de feedback ?
Question 2 :
  - Écrire un feedback que j'aimerais / j'aurais aimé recevoir.
  - Partager un à un les feedbacks, attention seulement avec ceux qui le souhaitent.
  - Les autres personnes (dont le coach) peuvent dire ce qu'elles en pensent.
Quatrième étape :
5. Prendre un engagement sur une expérience :
  - Comment je vais mettre en œuvre la pratique du feedback positif dans mon quotidien ?
  - Combien de temps ?
  - Quels vont être mes points d'attention ?
  - Quand est-ce que je fais le bilan de cette expérience ?

# Bilan

Ne négligez pas cette première étape dans l'apprentissage du feedback. Savoir donner des feedbacks positifs est essentiel dans la transformation de notre perception des autres. L'idée est d'apprendre à dire en quoi l'autre contribue à mon bien-être et en quoi cela est subjectif. Il permet de sortir du jeu de "qui a raison, qui a tort" et de réapprendre à voir ce qui va bien.

A vous de jouer, de tester et de personnaliser cet atelier. N'hésitez pas à nous faire des retours pour nous dire ce que vous avez essayé et comment vous avez vécu l'atelier.
