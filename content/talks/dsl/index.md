---
date: 2018-07-15
draft: false
title: "REx: Implémentation d'un DSL"
duration: 20 min
genre: Présentation
authors:
  - benoist_alexis
tags:
  - concepts
  - Software Craftsmanship
  - programmation
illustration:
  name: tools
  source: https://flic.kr/p/24AGZLS
abstract: |
  Comment écrire un DSL en python ?
ressources:
  - name: "Vidéo à PyCon FR 2018"
    file: https://www.youtube.com/watch?v=VeZzHQCe2CA
  - name: "Le code sur gitlab"
    file: https://gitlab.com/alexis.benoist/dsl
---

# Présentation détaillée
Un DSL (Domain Specific Langage) est un langage spécialisé capable de décrire des problématiques business avec un langage commun à l'ensemble des intervenants et donner de l'autonomie au métier.

Lors de ce retour d'expérience, nous discuterons de l'implémentation d'un
DSL. Ce DSL a pour but de donner de l'autonomie aux experts métier pour énoncer leurs règles.

Nous utiliserons ce cas d'étude pour implémenter un example et comprendre les DSLs.
